class CreateLessons < ActiveRecord::Migration[6.0]
  def change
    create_table :lessons do |t|
      t.integer :position
      t.text :title
      t.text :url
      t.string :duration
      t.references :course, null: false, foreign_key: true

      t.timestamps
    end
  end
end