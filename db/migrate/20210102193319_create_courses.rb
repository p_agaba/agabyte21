class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string :title
      t.integer :position
      t.string :slug
      t.string :duration
      t.text :overview
      t.text :objectives
      t.text :notes
      t.text :color


      t.timestamps
    end
  end
end
