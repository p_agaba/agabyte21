class Course < ApplicationRecord
  after_validation :set_slug, only: [:create, :update]
  
  include PgSearch::Model
  pg_search_scope :search,
    against: { title: 'A', overview: 'B', objectives: 'C', notes: 'D' },
    using: { tsearch: { dictionary: 'english' } }
  
  has_many :lessons
  
  validates :title, presence: true, length: { maximum: 100 }
  validates :position, presence: true
  validates :duration, presence: true  
  validates :overview, presence: true
  validates :objectives, presence: true
  validates :notes, presence: true
  validates :color, presence: true
  
  def to_param
    "#{id}-#{slug}"
  end
  
  private

    def set_slug
      self.slug = title.to_s.parameterize
    end

end