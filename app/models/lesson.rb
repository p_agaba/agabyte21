class Lesson < ApplicationRecord
  
  include PgSearch::Model
  pg_search_scope :search,
    against: %i[title],
    using: { tsearch: { dictionary: 'english' } }
  
  belongs_to :course
  
  validates :course_id, presence: true, numericality: { only_integer: true }
  validates :position, presence: true, numericality: { only_integer: true }
  validates :title, presence: true
  validates :url, presence: true
  validates :duration, presence: true
end