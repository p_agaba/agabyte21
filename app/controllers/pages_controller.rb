class PagesController < ApplicationController

  def about
    @title = "About Me"
  end
  
  def contact
    @title = "Contact Me"
  end
end