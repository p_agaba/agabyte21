Rails.application.routes.draw do
    
  root to: "courses#index"
  resources :courses
  resources :lessons
  get '/courses/:id/lessons', to: 'courses#lessons'
  get 'about', to: 'pages#about'
  # get 'contact', to: 'pages#contact'
  get 'login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
end
